#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-11-26
license: MIT

"""

from typing import Tuple, List

import random
import os
import sys
import subprocess

from colorama import Fore
from colorama import Style

from mpl_toolkits import mplot3d
import numpy as np
from scipy import stats

import matplotlib.pyplot as plt

from manim import *
from manim.utils.color import Colors

from LinearRegression import linearRegression, residual

config.background_color = WHITE

# ==============================================================================
#
# Examples
#
# ==============================================================================

class SquareToCircle(Scene):
    def construct(self):

        circle = Circle()  # create a circle
        circle.set_fill(PINK, opacity=0.5)  # set the color and transparency

        square = Square()
        square.set_color(BLACK)
        square.rotate(PI/4)

        self.play(Create(square))
        self.play(Transform(square, circle))
        self.play(FadeOut(square))  # show the circle on screen

class CoordSysExample(Scene):
    def construct(self):
        # the location of the ticks depends on the x_range and y_range.
        grid = Axes(
            x_range=[0, 1, 0.05],  # step size determines num_decimal_places.
            y_range=[0, 1, 0.05],
            x_length=9,
            y_length=5.5,
            axis_config={
                "numbers_to_include": np.arange(0, 1 + 0.1, 0.1),
                "font_size": 24,
            },
            tips=False,
        )

        # Labels for the x-axis and y-axis.
        y_label = grid.get_y_axis_label("y", edge=LEFT, direction=LEFT, buff=0.4)
        x_label = grid.get_x_axis_label("x")
        grid_labels = VGroup(x_label, y_label)

        graphs = VGroup()
        for n in np.arange(1, 20 + 0.5, 0.5):
            graphs += grid.plot(lambda x: x ** n, color=WHITE)
            graphs += grid.plot(
                lambda x: x ** (1 / n), color=WHITE, use_smoothing=False
            )

        # Extra lines and labels for point (1,1)
        graphs += grid.get_horizontal_line(grid.c2p(1, 1, 0), color=BLUE)
        graphs += grid.get_vertical_line(grid.c2p(1, 1, 0), color=BLUE)
        graphs += Dot(point=grid.c2p(1, 1, 0), color=YELLOW)
        graphs += Tex("(1,1)").scale(0.75).next_to(grid.c2p(1, 1, 0))
        title = Title(
            # spaces between braces to prevent SyntaxError
            r"Graphs of $y=x^{ {1}\over{n} }$ and $y=x^n (n=1,2,3,...,20)$",
            include_underline=False,
            font_size=40,
        )

        self.add(title, graphs, grid, grid_labels)

class GetZAxisLabelExample(ThreeDScene):
    def construct(self):
        ax = ThreeDAxes()
        lab = ax.get_z_axis_label(Tex("$z$-label"))
        self.set_camera_orientation(phi=2*PI/5, theta=PI/5)
        self.add(ax, lab)

class ArgMinExample(Scene):
    def construct(self):
        ax = Axes(
            x_range=[0, 10], y_range=[0, 100, 10], axis_config={"include_tip": False}
        )
        labels = ax.get_axis_labels(x_label="x", y_label="f(x)")

        t = ValueTracker(0)

        def func(x):
            return 2 * (x - 5) ** 2
        graph = ax.plot(func, color=MAROON)

        initial_point = [ax.coords_to_point(t.get_value(), func(t.get_value()))]
        dot = Dot(point=initial_point)

        dot.add_updater(lambda x: x.move_to(ax.c2p(t.get_value(), func(t.get_value()))))
        x_space = np.linspace(*ax.x_range[:2],200)
        minimum_index = func(x_space).argmin()

        self.add(ax, labels, graph, dot)
        self.play(t.animate.set_value(x_space[minimum_index]))
        self.wait()

class ArgMinExample(Scene):
    def construct(self):
        ax = Axes(
            x_range=[0, 10], y_range=[0, 100, 10], axis_config={"include_tip": False}
        )
        labels = ax.get_axis_labels(x_label="x", y_label="f(x)")

        t = ValueTracker(0)

        def func(x):
            return 2 * (x - 5) ** 2
        graph = ax.plot(func, color=MAROON)

        initial_point = [ax.coords_to_point(t.get_value(), func(t.get_value()))]
        dot = Dot(point=initial_point)

        dot.add_updater(lambda x: x.move_to(ax.c2p(t.get_value(), func(t.get_value()))))
        x_space = np.linspace(*ax.x_range[:2],200)
        minimum_index = func(x_space).argmin()

        self.add(ax, labels, graph, dot)
        self.play(t.animate.set_value(x_space[minimum_index]))
        self.wait()

class ThreeDCameraRotation(ThreeDScene):
    def construct(self):
        axes = ThreeDAxes()
        circle=Circle()
        self.set_camera_orientation(phi=75 * DEGREES, theta=30 * DEGREES)
        self.add(circle,axes)
        self.begin_ambient_camera_rotation(rate=0.1)
        self.wait()
        self.stop_ambient_camera_rotation()
        self.move_camera(phi=75 * DEGREES, theta=30 * DEGREES)
        self.wait()

class ThreeDSurfacePlot(ThreeDScene):
    def construct(self):
        resolution_fa = 42
        self.set_camera_orientation(phi=75 * DEGREES, theta=-30 * DEGREES)

        def param_gauss(u, v):
            x = u
            y = v
            sigma, mu = 0.4, [0.0, 0.0]
            d = np.linalg.norm(np.array([x - mu[0], y - mu[1]]))
            z = np.exp(-(d ** 2 / (2.0 * sigma ** 2)))
            return np.array([x, y, z])

        gauss_plane = Surface(
            param_gauss,
            resolution=(resolution_fa, resolution_fa),
            v_range=[-2, +2],
            u_range=[-2, +2]
        )

        gauss_plane.scale(2, about_point=ORIGIN)
        gauss_plane.set_style(fill_opacity=1,stroke_color=GREEN)
        gauss_plane.set_fill_by_checkerboard(ORANGE, BLUE, opacity=0.5)
        axes = ThreeDAxes()
        self.add(axes,gauss_plane)

class TwoMinDots(Scene):
    """
    https://towardsdatascience.com/take-your-python-visualizations-to-the-next-level-with-manim-ce9ad7ff66bf
    """
    def construct(self):
        np.random.seed(0)

        N = 9
        a = 2.0
        b = 0.0

        xp = np.arange(1, 10, (10-1)/N)
        yp = b + a * xp + 10.0 * np.random.random(N)

        # Create the axes object and labels
        ax = Axes(x_range=[0, 10], y_range=[0, 30, 10], axis_config={"include_tip": False})
        labels = ax.get_axis_labels(x_label="x", y_label="f(x)")

        # Example Function
        def func(x):
            return (0.1*(x**4)) - (x**2) + .5*x + 9 + 5*np.sin(x)

        # Plot the function on the axes
        graph = ax.plot(func, x_range=[0, 10], color=BLUE)

        # Create a pair of ValueTracker objetcs, one to track the X coordinate and one to track the Y coord
        # Value Tracker objects can be animated
        #xts = [ValueTracker(i) for i in range(-4,5)]
        #yts = [ValueTracker(30) for i in range(9)]
        xts = [ValueTracker(x) for x in xp]
        yts = [ValueTracker(0) for y in yp]

        # Make a list of initial points, create the dots, put them in a VGroup object
        # NOTE: when calling a point, we must define is as a property of our axes (ax) and use the c2p() (coord_to_point) method
        points = [[xts[i].get_value(), yts[i].get_value()] for i in range(N)]
        dots = [Dot(point=ax.c2p(*p), color=BLACK) for p in points]
        dot_group = VGroup(*dots)

        # Explicitly defining the updater fucntion in order to remove it later
        # Also need a function so an index can be input into the ValueTracker lists
        def updater_1(mobj, idx):
            mobj.add_updater(lambda x: x.move_to(ax.c2p(xts[idx].get_value(), yts[idx].get_value())))

        # Add updater, kind of a weird method here, but necessary because we also need the dots index in the value trackers
        for i, d in enumerate(dot_group):
            updater_1(d, i)

        # To play all animations at once we need them pre-looped and defined in a list
        #falling_dots = [y.animate.set_value(func(x.get_value())) for x,y in zip(xts,yts)]
        falling_dots = [y.animate.set_value(y1) for y1, y in zip(yp,yts)]

        # Add the (m)objects and animate changing the dot's y-value to the function line
        self.add(ax, labels, graph, dot_group)
        self.wait() # ** To add suspense **
        # Use the list of animations to play dots falling simultaneously
        self.play(LaggedStart(*falling_dots, lag_ratio=0.2))

        # Use below for loop to play dot animations one by one. I prefer the LaggedStart style though
          # for x,y in zip(xts, yts):
          #     self.play(y.animate.set_value(func(x.get_value())), run_time=0.25)

        # Clear the first updater to add a new one
        for dot in dot_group:
            dot.remove_updater(updater_1)

        # Define the updater for the second animation
        #def updater_2(mobj, idx):
        #    mobj.add_updater(lambda x: x.move_to(ax.c2p(xts[idx].get_value(), func(xts[idx].get_value()))))

        # Add updater to each dot as before
        #for i, d in enumerate(dot_group):
        #    updater_2(d, i)

        # Find indicies of minimum values of the function.
        # I know the relative max in the center of the function is @ 1.32, so split space there.
        #xspace_left = np.linspace(-4, 1.32, 300)
        #xspace_right = np.linspace(1.32, 4, 150)
        #min_idx_left = func(xspace_left).argmin()
        #min_idx_right = func(xspace_right).argmin()

        # Second animation step: change the x value tracker to the minimum point, updater will move dot along line.
        # Create separate animations based on if the value is on either side of the relative maximum
        #rolling_dots_left = [x.animate.set_value(xspace_left[min_idx_left]) for x in xts if x.get_value() < 1.32]
        #rolling_dots_right = [x.animate.set_value(xspace_right[min_idx_right]) for x in xts if x.get_value() >= 1.32]
        #rolling_dots = rolling_dots_left + rolling_dots_right

        # Play the second animations
        #self.play(LaggedStart(*rolling_dots, lag_ratio=0.03), run_time=2.5)
        self.wait(2)

class FunctionTracker(Scene):
    def construct(self):
        # f(x) = x**2
        fx = lambda x: x.get_value()**2
        # ValueTrackers definition
        x_value = ValueTracker(0)
        fx_value = ValueTracker(fx(x_value))
        # DecimalNumber definition
        x_tex = DecimalNumber(x_value.get_value()).add_updater(lambda v: v.set_value(x_value.get_value()))
        fx_tex = DecimalNumber(fx_value.get_value()).add_updater(lambda v: v.set_value(fx(x_value)))
        # TeX labels definition
        x_label = TexMobject("x = ")
        fx_label = TexMobject("x^2 = ")
        # Grouping of labels and numbers
        group = VGroup(x_tex,fx_tex,x_label,fx_label).scale(2.6)
        VGroup(x_tex, fx_tex).arrange_submobjects(DOWN,buff=3)
        # Align labels and numbers
        x_label.next_to(x_tex,LEFT, buff=0.7,aligned_edge=x_label.get_bottom())
        fx_label.next_to(fx_tex,LEFT, buff=0.7,aligned_edge=fx_label.get_bottom())

        self.add(group.move_to(ORIGIN))
        self.wait(3)
        self.play(
            x_value.set_value,30,
            rate_func=linear,
            run_time=10
            )
        self.wait()
        self.play(
            x_value.set_value,0,
            rate_func=linear,
            run_time=10
            )
        self.wait(3)

# ==============================================================================
#
# Main
#
# ==============================================================================

if __name__=='__main__':
    print("Hello :)")
    print("Useage:")
    print("manim -pql scene.py SquareToCircle")


