#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-11-26
license: MIT

"""

from typing import Tuple, List

import random

from colorama import Fore
from colorama import Style

import numpy as np
from scipy import stats

import matplotlib.pyplot as plt

# ==============================================================================
#
# Linear regression implementation
#
# ==============================================================================

def linearRegression(x: np.ndarray, y: np.ndarray) -> Tuple[float, float]:
    """
        Fit a line to a set of points in a least-squares fasion.
        Returns the slope a and the intercept b.
        y = a*x + b
    """
    N = len(x)
    A = np.sum(x*x)
    B = np.sum(x)
    C = np.sum(x*y)
    D = np.sum(y)

    a = (B*D - C*N) / (B*B - A*N)
    b = (B*C - D*A) / (B*B - A*N)

    return a, b

def residual(x: np.ndarray, y: np.ndarray, a, b) -> Tuple[float, float, float]:
    """
        Returns the error residual between data and line given by a and b
        and the gradient in a and b.
        e, eda, edb
    """

    e = 0.0
    eda = 0.0
    edb = 0.0
    for i in range(len(x)):
        v = a*x[i] + b - y[i]

        e += v*v
        eda += 2*x[i]*v
        edb += 2*v

    return e, eda, edb

# ==============================================================================
#
# Test against scipy implementation
#
# ==============================================================================

def testLinearRegression(print_results=False, plot_data=False) -> bool:
    """
        Generate a noisy data set with linear distribution and fit a line.
        The parameters for the slope and the offset are compared to the
        values obtained by scipy implementation of linear regression.
    """

    N = random.randint(10, 100)
    offset = random.randint(-50, 50)
    dx = (100+1) / N

    a = random.randint( -20,  20)
    b = random.randint(-100, 100)

    x = np.arange(offset, offset+100, dx)
    y = b + a * x + 50 * a * np.random.random(N)

    a0, b0 = linearRegression(x, y)
    a1, b1, _, _, _, = stats.linregress(x, y)
    success = abs(a0-a1)<1e-8 and abs(b0-b1)<1e-8

    if print_results:
        print(f"Slope a:       {a0:7.2f} - {a1:7.2f}")
        print(f"Intercept b:   {b0:7.2f} - {b1:7.2f}")
        print(f"Nr of samples: {N:>7}")

    if plot_data:
        plt.scatter(x, y)
        plt.plot([x[0], x[-1]], [a0*x[0]+b0, a0*x[-1]+b0], c='red')
        plt.show()

    return success

if __name__=='__main__':
    success = True
    for i in range(0, 1000):
        if i==0:
            success = testLinearRegression(print_results=True, plot_data=True)
        else:
            success = testLinearRegression()
        if not success:
            break

    if success:
        msg = f"Success! Linear regression code gave same result as scipy implementation for all cases!\n"
        msg = f'{Fore.LIGHTBLUE_EX}{msg}{Style.RESET_ALL}'
        print(msg)
    else:
        msg = f"Failed! Linear regression code did not give same result as scipy implementation for all cases!\n"
        msg = f'{Fore.LIGHTRED_EX}{msg}{Style.RESET_ALL}'
        print(msg)
