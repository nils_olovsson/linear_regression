#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-11-26
license: MIT

"""

import os

from typing import Tuple, List

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

from LinearRegression import linearRegression

# ------------------------------------------------------------------------------
# Part 1 - Gamma
# ------------------------------------------------------------------------------
bkg_1      = 20
density_Pb = 11.35 # g/cm^3

# B
B = {}
B['thickness'] = np.array([0.0, 1, 2, 3.2, 4.2, 5.2, 6.4, 7.4, 9.6, 16])
B['counts'] = np.array([1000, 200, 140, 90, 70, 65, 70, 60, 60, 50])
B['background'] = bkg_1
B['density'] = density_Pb

B['thickness'] = B['thickness'][0:-1]
B['counts'] = B['counts'][0:-1]
B['fitness-limit'] = [0,5]

def computeAttenuationCoeff(data, title):
    """
        The natural logarithm is used for calculating the values
        so we use this when we fit the line to find the attenuation
        coefficient.
    """

    thickness = data['thickness']
    counts = data['counts']
    density = data['density']
    background = data['background']
    fitlim = data['fitness-limit']

    log_counts = np.log(counts[fitlim[0]:fitlim[1]])
    my, offset = linearRegression(thickness[fitlim[0]:fitlim[1]], log_counts)

    ht0_line = np.log(2) / -my        # [mm]
    ht1_line = 0.1*ht0_line * density # [g/cm^2]

    print('#------------------------------------------------')
    print('# {}'.format(title))
    print('#------------------------------------------------')
    print('Offset: {}'.format(offset))
    print(f'Linear absorbtion coefficient:\n    {np.abs(my):.3f} [Counts per second/mm]')
    print(f'Half-thickness ln ({title}):       \n    {ht0_line:.3f} [mm]\n    {ht1_line:.3f} [g/cm^2]')
    print('')

def analysisPart1(data, title, directory):
    """
        Analysis from scintillator detector (gamma and xrays)
        We use log10 when plotting the results.
    """

    thickness = data['thickness']
    counts = data['counts']
    density = data['density']
    background = data['background']
    fitlim = data['fitness-limit']

    computeAttenuationCoeff(data, title)

    # Compute log values of counts, fit line and get slope (linear attenuation coeff.) and half-width
    log_counts = np.log10(counts[fitlim[0]:fitlim[1]])
    my, offset = linearRegression(thickness[fitlim[0]:fitlim[1]], log_counts)

    v = offset + my*thickness[fitlim[0]:fitlim[1]]
    v = 10.0**v

    ht0 = (np.log10(0.5*v[0]) - offset)/my # [mm]
    ht1 = 0.1*ht0 * density                # [g/cm^2]

    # Plot
    plt.figure(figsize=(5, 3))
    ax = plt.gca()
    #plt.scatter(thickness, counts, marker='.', s=10.0,  color='tab:blue', label='1. Measurments')
    plt.scatter(thickness, counts,  color='tab:blue', label='1. Measurments')
    plt.plot(thickness[fitlim[0]:fitlim[1]], v, label='2. Line fit to data', color='tab:orange')
    plt.yscale('log')

    x = thickness[fitlim[1]:]
    y = counts[fitlim[1]:]
    plt.scatter(x, y, marker='s', color='green', label='3. Measurements\nnot used for line fit')

    # --------------------------------------------
    # Half thickness indicator
    # --------------------------------------------
    linestyle = 'dotted'
    label = '4. Half thickness'
    color = 'black'
    #p1 = [0, 0.5*counts[0]]
    #p2 = [ht0, 0.5*counts[0]]
    #p3 = [ht0, 0]

    p1 = [0, 0.5*v[0]]
    p2 = [ht0, 0.5*v[0]]
    p3 = [ht0, 0]

    l1 = mlines.Line2D([p1[0],p2[0]], [p1[1],p2[1]])
    l1.set_color(color)
    l1.set_linestyle(linestyle)
    l1.set_label(label)
    ax.add_line(l1)

    l2 = mlines.Line2D([p2[0],p3[0]], [p2[1],p3[1]])
    l2.set_color(color)
    l2.set_linestyle(linestyle)
    #l2.set_label(label)
    ax.add_line(l2)

    #plt.legend()
    handles, labels = ax.get_legend_handles_labels()
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    ax.legend(handles, labels)

    ax.set_ylabel('Intensity [Counts per second]')
    ax.set_xlabel('Thickness [mm]')

    major_ticks = np.arange(0, 12, 2)
    minor_ticks = np.arange(0, 11, 1)
    ax.set_xticks(major_ticks)
    ax.set_xticks(minor_ticks, minor=True)

    fname = os.path.join(directory, f'example_radiation_{title}.svg')
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

# ==============================================================================
# Main
# ==============================================================================
if __name__ == '__main__':
    directory = os.path.join('.', 'figures')
    if not os.path.isdir(directory):
        os.mkdir(directory, 0o0755)
    analysisPart1(B, 'B', directory)
