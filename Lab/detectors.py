
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

# ------------------------------------------------------------------------------
# Part 1 - Gamma
# ------------------------------------------------------------------------------
bkg_1 = 20
bkg_2 = 1

density_Al      = 2.7   # g/cm^3
density_Pb      = 11.35 # g/cm^3
density_plastic = 1.2   # g/cm^3

# A
A = {}
A['thickness'] = np.array([0.0, 1.0, 2.0, 3.0, 3.2, 5.0, 6.0, 7.0, 8.0, 9.0, 11, 12, 13, 14])
A['counts'] = np.array([1000, 750, 550, 400, 300, 200, 150, 120, 100, 80, 50, 40, 30, 30])
A['background'] = bkg_1
A['density'] = density_Al

#A['thickness'] = A['thickness'][0:-1]
#A['counts'] = A['counts'][0:-1]
A['fitness-limit'] = [0,-1]

# B
B = {}
B['thickness'] = np.array([0.0, 1, 2, 3.2, 4.2, 5.2, 6.4, 7.4, 9.6, 16])
B['counts'] = np.array([1000, 200, 140, 90, 70, 65, 70, 60, 60, 50])
B['background'] = bkg_1
B['density'] = density_Pb

B['thickness'] = B['thickness'][0:-1]
B['counts'] = B['counts'][0:-1]
B['fitness-limit'] = [0,5]

# C
C = {}
C['thickness']  = np.array([0, 1.0, 2.0, 3.0, 4.5, 5, 6, 7, 8, 9])
C['counts']     = np.array([500, 220, 100, 50, 30, 20, 15, 10, 5, 5])
C['background'] = bkg_2
C['density'] = density_plastic
C['fitness-limit']  = [0,8]

# D (20um sheets of plastic film)
D = {}
D['sheets']         = np.array([0, 1, 2, 3, 4])
D['counts']         = np.array([500, 250, 120, 50, 5])
D['background']     = bkg_2
D['thickness']      = 20   # um (per sheet)
D['stopping-power'] = 2.87 # mg/cm^2
D['fitness-limit']  = [0,len(D['counts'])]

def computeAttenuationCoeff(data, title):
    """
    """
    
    thickness = data['thickness']
    counts = data['counts']
    density = data['density']
    background = data['background']
    fitlim = data['fitness-limit']
    
    log_counts = np.log(counts[fitlim[0]:fitlim[1]])
    my, offset, _, _, _ = stats.linregress(thickness[fitlim[0]:fitlim[1]], log_counts)

    #v = offset + my*thickness[fitlim[0]:fitlim[1]]
    #v = 10.0**v

    #ht0 = (np.log(0.5*counts[0]) - offset)/my # [mm]
    #ht1 = 0.1*ht0 * density                   # [g/cm^2]
    
    #print('#------------------------------------------------')
    #print('# {} Natural Log'.format(title))
    #print('#------------------------------------------------')
    print('Offset: {}'.format(offset))
    print('Linear absorbtion coefficient:\n    {} [Counts per second/mm]'.format(np.abs(my)))
    #print('Half-thickness ({}):\n    {} [mm]\n    {} [g/cm^2]'.format(title, ht0, ht1))
    print('')

def analysisPart1(data, title, show=True):
    """
        Analysis from scintillator detector (gamma and xrays)
    """


    thickness = data['thickness']
    counts = data['counts']
    density = data['density']
    background = data['background']
    fitlim = data['fitness-limit']

    # Compute log values of counts, fit line and get slope (linear attenuation coeff.) and half-width
    log_counts = np.log10(counts[fitlim[0]:fitlim[1]])
    my, offset, _, _, _ = stats.linregress(thickness[fitlim[0]:fitlim[1]], log_counts)

    v = offset + my*thickness[fitlim[0]:fitlim[1]]
    v = 10.0**v

    ht0 = (np.log10(0.5*counts[0]) - offset)/my # [mm]
    ht1 = 0.1*ht0 * density                     # [g/cm^2]
    
    print('#------------------------------------------------')
    print('# {}'.format(title))
    print('#------------------------------------------------')
    computeAttenuationCoeff(data, title)
    #print('Offset: {}'.format(offset))
    #print('Linear absorbtion coefficient:\n    {} [Counts per second/mm]'.format(np.abs(my)))
    print('Half-thickness ({}):\n    {} [mm]\n    {} [g/cm^2]'.format(title, ht0, ht1))
    print('')

    # Plot
    plt.figure(figsize=(4.5, 3.2))
    ax = plt.gca()
    plt.scatter(thickness, counts, marker='x', color='red', label='1. Measurments')
    plt.plot(thickness[fitlim[0]:fitlim[1]], v, label='2. Line fit to data')
    plt.yscale('log')

    x = thickness[fitlim[1]:]
    y = counts[fitlim[1]:]
    plt.scatter(x, y, marker='s', color='green', label='3. Measurements\nnot used for line fit')

    linestyle = 'dotted'
    label = '4. Half thickness'
    color = 'black'

    p1 = [0, 0.5*counts[0]]
    p2 = [ht0, 0.5*counts[0]]
    p3 = [ht0, 0]

    l1 = mlines.Line2D([p1[0],p2[0]], [p1[1],p2[1]])
    l1.set_color(color)
    l1.set_linestyle(linestyle)
    l1.set_label(label)
    ax.add_line(l1)
    
    #plt.legend()
    handles, labels = ax.get_legend_handles_labels()
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    ax.legend(handles, labels)
    
    l2 = mlines.Line2D([p2[0],p3[0]], [p2[1],p3[1]])
    l2.set_color(color)
    l2.set_linestyle(linestyle)
    #l2.set_label(label)
    ax.add_line(l2)

    plt.ylabel('Intensity [Counts per second]')
    plt.xlabel('Thickness [mm]')

    plt.tight_layout(.5)
    
    fname = '../img/figure_{}.pdf'.format(title)
    plt.savefig(fname)

    if show:
        plt.show()
    plt.close()

# ------------------------------------------------------------------------------
# Part 2 - Beta
# ------------------------------------------------------------------------------

def analysisPart2(data, title, show=True):
    """
        Analysis from Geiger-Muller detector (Beta)
    """
    
    thickness = data['thickness']
    counts = data['counts']
    density = data['density']
    background = data['background']
    fitlim = data['fitness-limit']

    if 'stopping-power' in data.keys():
        print('hello')

    # Compute log values of counts, fit line and get slope to find max energy
    log_counts = np.log10(counts[fitlim[0]:fitlim[1]])
    k, m, _, _, _ = stats.linregress(thickness[fitlim[0]:fitlim[1]], log_counts)

    r  = (np.log10(background) - m)/k # [mm]
    dr = 1000*0.1*density*r           # [mg/cm^2]

    t = np.zeros(len(thickness[fitlim[0]:fitlim[1]])+1)
    t[-1] = r
    v = m + k*t
    v = 10.0**v

    print('#------------------------------------------------')
    print('# {}'.format(title))
    print('#------------------------------------------------')
    print('Range:\n    {} [mm]\n    {} [mg/cm^2]\n'.format(r, dr))

    # Plot
    plt.figure(figsize=(4.5, 3.2))
    ax = plt.gca()
    plt.scatter(thickness, counts, marker='x', color='red', label='1. Measurements')
    plt.plot(t, v, label='2. Line fit to data')
    plt.scatter(r, background, s=40, facecolors='none', edgecolors='blue', label='3. Calculated range')
    plt.yscale('log')
    plt.ylim(bottom=0.9)
    
    x = thickness[fitlim[1]:]
    y = counts[fitlim[1]:]
    plt.scatter(x, y, marker='s', color='green', label='4. Measurements\nnot used for line fit')

    label = '5. Background activity'
    linestyle = 'dotted'
    color = 'black'

    t0 = thickness[0]
    t1 = max(thickness[-1], r)
    p1 = [t0, background]
    p2 = [t1, background]

    l1 = mlines.Line2D([p1[0],p2[0]], [p1[1],p2[1]])
    l1.set_color(color)
    l1.set_linestyle(linestyle)
    l1.set_label(label)
    ax.add_line(l1)

    plt.ylabel('Intensity[Counts per second]')
    plt.xlabel('Thickness [mm]')
    
    #plt.legend()
    handles, labels = ax.get_legend_handles_labels()
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    ax.legend(handles, labels)
    
    plt.tight_layout(.5)
    
    fname = '../img/figure_{}.pdf'.format(title)
    plt.savefig(fname)

    if show:
        plt.show()
    plt.close()

# ------------------------------------------------------------------------------
# Part 3 - Beta (with plastic sheets)
# ------------------------------------------------------------------------------

def analysisPart3(data, title, show=True):
    """
        Analysis from Geiger-Muller detector (Beta)
    """
    
    sheets = data['sheets']
    counts = data['counts']
    sp = data['stopping-power']
    background = data['background']
    fitlim = data['fitness-limit']

    # Compute log values of counts, fit line and get slope to find max energy
    log_counts = np.log10(counts[fitlim[0]:fitlim[1]])
    k, m, _, _, _ = stats.linregress(sheets[fitlim[0]:fitlim[1]], log_counts)

    r  = (np.log10(background) - m)/k # [number of sheets]
    dr = sp*r                         # [mg/cm^2]

    t = np.zeros(len(sheets[fitlim[0]:fitlim[1]])+1)
    t[-1] = r
    v = m + k*t
    v = 10.0**v

    print('#------------------------------------------------')
    print('# {}'.format(title))
    print('#------------------------------------------------')
    print('Range:\n    {} [number of sheets]\n    {} [mg/cm^2]\n'.format(r, dr))

    # Plot
    plt.figure(figsize=(5, 4))
    ax = plt.gca()
    plt.scatter(sheets, counts, marker='x', color='red', label='1. Measurements')
    plt.plot(t, v, label='2. Line fit to data')
    plt.scatter(r, background, s=40, facecolors='none', edgecolors='blue', label='3. Calculated range')
    plt.yscale('log')
    plt.ylim(bottom=0.9)
    
    x = sheets[fitlim[1]:]
    y = counts[fitlim[1]:]
    plt.scatter(x, y, marker='s', color='green', label='4. Measurements\nnot used for line fit')

    label = '5. Background activity'
    linestyle = 'dotted'
    color = 'black'

    t0 = sheets[0]
    t1 = max(sheets[-1], r)
    p1 = [t0, background]
    p2 = [t1, background]

    l1 = mlines.Line2D([p1[0],p2[0]], [p1[1],p2[1]])
    l1.set_color(color)
    l1.set_linestyle(linestyle)
    l1.set_label(label)
    ax.add_line(l1)

    plt.ylabel('Intensity [Counts per second]')
    plt.xlabel('Number of plastic sheets')
    
    #plt.legend()
    handles, labels = ax.get_legend_handles_labels()
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    ax.legend(handles, labels)

    plt.tight_layout(.5)
    
    fname = '../img/figure_{}.pdf'.format(title)
    plt.savefig(fname)

    if show:
        plt.show()
    plt.close()

# ============================================================================== 
# Main
# ============================================================================== 
if __name__ == '__main__':
    analysisPart1(A, 'A', show=False)
    analysisPart1(B, 'B', show=False)
    
    analysisPart2(C, 'C', show=False)
    analysisPart3(D, 'D', show=False)
