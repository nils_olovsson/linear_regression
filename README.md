**Linear regression**
A python implementation of the linear regression method for fitting
a line to a set of data points.

The scripts can be used to recreate the figures used in the blog post.

Blog post:
http://www.all-systems-phenomenal.com/articles/linear_regression

**License: MIT**
