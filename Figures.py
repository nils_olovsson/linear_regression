#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-11-26
license: MIT

"""

from typing import Tuple, List

import os
import random

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.lines as mlines

from LinearRegression import linearRegression

def makeHeaderFigure(directory):
    N = 11

    a = 2.0
    b = 3.0

    np.random.seed(42)
    x = np.arange(-10, 10+2, 2)
    y = b + a * x + 15.0 * np.random.random(N)

    print(len(x))
    print(len(y))

    a0, b0 = linearRegression(x, y)

    fig = plt.figure(figsize=(8,2))
    ax = fig.gca()

    ax.scatter(x, y)
    ax.plot([x[0], x[-1]], [a0*x[0]+b0, a0*x[-1]+b0], c='tab:orange', solid_capstyle='round')

    ax.plot([x[0], x[-1]], [(a0+2)*x[0]+b0, (a0+2)*x[-1]+b0], c='tab:green', linestyle='--')
    ax.plot([x[0], x[-1]], [a0*x[0]+b0+10, a0*x[-1]+b0+10], c='tab:purple', linestyle=':')

    ax.set_axis_off()

    plt.savefig(os.path.join(directory, 'linreg.svg'), bbox_inches='tight')
    #plt.show()
    plt.close()

    fig = plt.figure(figsize=(2,2), dpi=150)
    ax = fig.gca()

    ax.scatter(x, y)
    ax.plot([x[0], x[-1]], [a0*x[0]+b0, a0*x[-1]+b0], c='tab:orange', linewidth=2.0, solid_capstyle='round')

    ax.plot([x[0], x[-1]], [(a0+2)*x[0]+b0, (a0+2)*x[-1]+b0], c='tab:green', linestyle='--', linewidth=2.0, solid_capstyle='round')
    ax.plot([x[0], x[-1]], [a0*x[0]+b0+10, a0*x[-1]+b0+10], c='tab:purple', linestyle=':', linewidth=2.0, solid_capstyle='round')

    ax.set_axis_off()

    plt.savefig(os.path.join(directory, 'repo-linreg.png'), bbox_inches='tight')
    #plt.savefig(os.path.join(directory, 'repo-linreg.png'))
    #plt.show()
    plt.close()

def makeEuclidianDistanceFigure(directory):
    """
    """
    N = 11

    a = 0.3
    b = 0.0

    np.random.seed(42)
    x = np.arange(-10, 10+2, 2)
    y = b + a * x + 5.0 * (np.random.random(N)-0.5)

    #print(len(x))
    #print(len(y))

    a0, b0 = linearRegression(x, y)

    fig = plt.figure(figsize=(6,2))
    ax = fig.gca()

    ax.scatter(x[1:-1], y[1:-1], s=50.0, zorder=5)
    ax.plot([x[0], x[-1]], [a0*x[0]+b0, a0*x[-1]+b0], c='tab:orange', linewidth=4.0, solid_capstyle='round', zorder=5)

    def normalize(v):
        return (1.0/np.linalg.norm(v)) * v

    def closestPoint(q0, t, n, p):
        x = np.dot(t, (p-q0))
        q = q0 + x*t
        return q

    def distance(q0, t, n, p):
        diff = p - q0;
        param = np.dot(t, diff);
        return q0 + param*t;

    l_vertical = None
    l_closest = None

    q0 = np.array([0, b0])
    q1 = np.array([2, b0+a0*2])
    t = q0 - q1
    t = normalize(t)
    n = np.array([-t[1], t[0]])
    for i in range(1,N-1):
        p0 = np.array([x[i], y[i]])
        p1 = np.array([x[i], b0+ a0*x[i]])
        l = mlines.Line2D([p0[0],p1[0]], [p0[1],p1[1]])
        l.set_color('tab:green')
        l.set_linestyle(':')
        l.set_zorder(4)
        ax.add_line(l)
        l_vertical = l

        p0 = np.array([x[i], y[i]])
        p1 = closestPoint(q0, t, n, p0)
        #ax.scatter(p1[0], p1[1], s=50.0, marker='+', c='tab:green', zorder=15)

        l = mlines.Line2D([p0[0],p1[0]], [p0[1],p1[1]])
        l.set_color('tab:red')
        l.set_linestyle('--')
        l.set_zorder(4)
        ax.add_line(l)
        l_closest = l

        if i==1:
            l_vertical.set_label("Vertical distance")
            l_closest.set_label("Closest distance")

    handles, labels = ax.get_legend_handles_labels()
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    legend = ax.legend(handles=handles,
                       labels=labels,
                       #bbox_to_anchor=(1.05, -0.05),
                       #title='Distances',
                       loc='upper left')
    ax.add_artist(legend)

    L = 12
    ax.set_xlim(-L,   L)
    ax.set_ylim(-L/3, L/3)

    ax.set_axis_off()

    plt.savefig(os.path.join(directory, 'lingeom.svg'), bbox_inches='tight')
    #plt.show()
    plt.close()

if __name__=='__main__':

    directory = os.path.join('.', 'figures')
    if not os.path.isdir(directory):
        os.mkdir(directory, 0o0755)

    makeHeaderFigure(directory)
    makeEuclidianDistanceFigure(directory)
