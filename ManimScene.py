#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-11-26
license: MIT

"""

from typing import Tuple, List

import random
import os
import sys
import subprocess

from colorama import Fore
from colorama import Style

from mpl_toolkits import mplot3d
import numpy as np
from scipy import stats

import matplotlib.pyplot as plt

from manim import *
from manim.utils.color import Colors

from LinearRegression import linearRegression, residual

config.background_color = WHITE

# ==============================================================================
#
# Linear regression example
#
# ==============================================================================

class RegLine:
    def __init__(self, a, b, x0, x1):
        self.a = a
        self.b = b
        self.x0 = x0
        self.x1 = x1
        self.y0 = a*x0 + b
        self.y1 = a*x1 + b

    def testTransform(self, ax):
        p0 = self.getPoint0(ax)
        p1 = self.getPoint1(ax)

        print("# ==========================================================")
        print("Test")
        print(f"{self.x0} -> {self.y0}")
        print(f"{self.x1} -> {self.y1}")
        print(ax.p2c(p0.get_center()))
        print(ax.p2c(p1.get_center()))
        print("End of test")

    def getPoint0(self, ax):
        return Dot(point=ax.c2p(self.x0, self.y0, 0))

    def getPoint1(self, ax):
        return Dot(point=ax.c2p(self.x1, self.y1, 0))

    def getLine(self, ax):
        p0 = self.getPoint0(ax)
        p1 = self.getPoint1(ax)
        return Line(p0.get_center(), p1.get_center(), color=BLACK)

class LinearRegression2D(Scene):
    """
        2D plot of linear regression example. Wiggle the fitted line around a
        little.
        Made with manim version 0.12.0
    """

    def construct(self):
        colors = {}
        colors['dot']     = color.rgb_to_color([ 31/255, 119/255, 180/255])
        colors['badline'] = color.rgb_to_color([ 44/255, 160/255,  44/255])
        colors['regline'] = color.rgb_to_color([255/255, 127/255,  14/255])

        np.random.seed(1)

        N = 21
        a = 8.0
        b = -10.0

        x_points = np.arange(1, 10, (10-1)/21)
        y_points = b + a * x_points + 50.0 * np.random.random(N)

        #print(len(x_points))
        #print(len(y_points))

        a0, b0 = linearRegression(x_points, y_points)
        e0, eda0, edb0 = residual(x_points, y_points, a0, b0)
        #print(f"a0: {a0:.3f} - b0: {b0:.3f}")

        lines = []
        lines.append(RegLine(a,   b,   1, 10))
        lines.append(RegLine(a,   b+5, 1, 10))
        lines.append(RegLine(a,   b-5, 1, 10))
        lines.append(RegLine(a,   b,   1, 10))
        lines.append(RegLine(a+2, b,   1, 10))
        lines.append(RegLine(a-2, b,   1, 10))
        lines.append(RegLine(a0,  b0,  1, 10))

        """
        e, eda, edb = residual(x_points, y_points, a0, b0)
        print(f"residual: {e/e0:.3f} - {eda:.3f} : {edb:.3f}")
        e, eda, edb = residual(x_points, y_points, a0+0.1, b0)
        print(f"residual: {e/e0:.3f} - {eda:.3f} : {edb:.3f}")
        e, eda, edb = residual(x_points, y_points, a0-0.1, b0)
        print(f"residual: {e/e0:.3f} - {eda:.3f} : {edb:.3f}")
        e, eda, edb = residual(x_points, y_points, a0, b0+0.1)
        print(f"residual: {e/e0:.3f} - {eda:.3f} : {edb:.3f}")
        e, eda, edb = residual(x_points, y_points, a0, b0-0.1)
        print(f"residual: {e/e0:.3f} - {eda:.3f} : {edb:.3f}")

        print(f"a0: {a0:.3f} b0: {b0:.3f}")

        a_points = np.arange(a0-5, a0+5, 1)
        b_points = np.arange(b0-6, b0+6, 2)
        grid2d = np.zeros((len(b_points), len(a_points)))
        for i in range(len(a_points)):
            for j in range(len(b_points)):
                e, eda, edb = residual(x_points, y_points, a_points[i], b_points[j])
                grid2d[j,i] = np.log10(e) # np.log10(e/e0) #np.log10(e)
                #grid2d[i,j] = np.log10(np.linalg.norm(np.array([eda, edb])))

        Z = grid2d

        p = np.argmin(grid2d, axis=0)
        print(p)

        cm = plt.cm.coolwarm
        A, B = np.meshgrid(a_points, b_points)
        norm = plt.Normalize(Z.min(), Z.max())
        colors = cm(norm(Z))
        rcount, ccount, _ = colors.shape

        fig = plt.figure(figsize=(20, 10))
        ax   = fig.gca(projection='3d')
        surf = ax.plot_surface(A, B, Z, cmap=cm, rstride=1, cstride=1,
                facecolors=colors, alpha=0.75, shade=False, linewidths=1, zorder=4)
        surf.set_edgecolor([0.25, 0.25, 0.25, 1])
        ax._axis3don = True

        ax.scatter(a0, b0, np.log10(e0), marker='o')

        ax.set_xlabel("a")
        ax.set_ylabel("b")

        #cset = ax.contour(A, B, Z, zdir='x', offset=-4, cmap=cm, levels=10)
        #cset = ax.contour(A, B, Z, zdir='y', offset=4, cmap=cm, levels=10)

        #ax.plot_surface(a_points, b_points, grid2d, cmap='viridis', edgecolor='none')
        #ax.set_title('Surface plot')
        #plt.imshow(grid2d, extent=[a_points[0], a_points[-1], b_points[0], b_points[-1]], cmap='jet')
        #plt.imshow(grid2d)
        #plt.xticks(a_points)
        #plt.yticks(b_points)
        #plt.scatter(a0, b0, marker='o')
        #plt.scatter(p[0], p[1], marker='o')
        plt.show()
        """

        ax = Axes(
            x_range=[0, 10, 1], y_range=[0, 100, 10], axis_config={"include_tip": False},
            color=BLACK
        )
        labels = ax.get_axis_labels(x_label="x", y_label="y=f(x)")
        labels.set_color(BLACK)
        ax.set_color(BLACK)

        xts = [ValueTracker(x_points[i]) for i in range(N)]
        yts = [ValueTracker(0.0) for i in range(N)]

        # Make a list of initial points, create the dots, put them in a VGroup object
        # NOTE: when calling a point, we must define is as a property of our axes (ax) and use the c2p() (coord_to_point) method
        points = [[xts[i].get_value(), yts[i].get_value()] for i in range(N)]
        dots = [Dot(point=ax.c2p(*p), color=colors['dot']) for p in points]
        dot_group = VGroup(*dots)

        # Explicitly defining the updater fucntion in order to remove it later
        # Also need a function so an index can be input into the ValueTracker lists
        def updater_1(mobj, idx):
            mobj.add_updater(lambda x: x.move_to(ax.c2p(xts[idx].get_value(), yts[idx].get_value())))

        # Add updater, kind of a weird method here, but necessary because we also need the dots index in the value trackers
        for i, d in enumerate(dot_group):
            updater_1(d, i)

        # To play all animations at once we need them pre-looped and defined in a list
        #falling_dots = [y.animate.set_value(func(x.get_value())) for x,y in zip(xts,yts)]
        rising_dots = [y.animate.set_value(y1) for y1, y in zip(y_points, yts)]

        mlines = []
        for l in lines:
            mline = l.getLine(ax)
            mline.set_color(colors['badline'])
            mlines.append(mline)
        mlines[-1].set_color(colors['regline'])

        e,_,_ = residual(x_points, y_points, a, b)
        dec_e = DecimalNumber(
            e,
            show_ellipsis=False,
            num_decimal_places=2,
            include_sign=False,
            color=BLACK,
        )
        dec_a = DecimalNumber(
            a,
            show_ellipsis=False,
            num_decimal_places=2,
            include_sign=False,
            color=BLACK,
        )
        dec_b = DecimalNumber(
            b,
            show_ellipsis=False,
            num_decimal_places=2,
            include_sign=False,
            color=BLACK,
        )
        #square = Square().to_edge(UP)

        #x_offset = ax.p2c(ax.c2p(0,0,0))
        #print('X offset')
        #print(x_offset)
        self.current_line = mlines[0]
        self.current_lline = lines[0]
        def lineUpdater(info=False):
            p0 = ax.p2c(self.current_line.get_start())
            p1 = ax.p2c(self.current_line.get_end())
            a = (p1[1] - p0[1])/(p1[0] - p0[0])
            b = p0[1] - p0[0]*a
            return a, b

        def lineUpdaterE(d):
            a, b = lineUpdater()
            e, _, _  = residual(x_points, y_points, a, b)
            d.set_value(e/e0 - 1)
            #d.set_value(e)
            #d.set_num_decimal_places(2)

        def lineUpdaterA(d):
            a, b = lineUpdater()
            d.set_value(a)
            #d.num_decimal_places = 2

        def lineUpdaterB(d):
            a, b = lineUpdater()
            d.set_value(b)
            d#.num_decimal_places = 2

        self.current_line = mlines[0]
        lineUpdaterE(dec_e)
        lineUpdaterA(dec_a)
        lineUpdaterB(dec_b)

        text_eq= Tex(r"$y = a \cdot x + b$", color=BLACK)
        text_eq.next_to(ax.c2p(0.5, 90), RIGHT)
        text_a = Tex(r"$a = $", color=BLACK)
        text_a.next_to(ax.c2p(0.5, 80), RIGHT)
        text_b = Tex(r"$b = $", color=BLACK)
        text_b.next_to(ax.c2p(0.5, 70), RIGHT)
        text_e = Tex(r"$\epsilon_{\text{rel}} = $", color=BLACK)
        text_e.next_to(ax.c2p(0.5, 60), RIGHT)

        text_0 = Tex(r"Try some values\\of $a$ and $b$.", color=BLACK)#, font_size=32)
        text_1 = Tex(r"Set $a$ and $b$\\to values found\\by linear regression.", color=BLACK)#, font_size=32)
        text_0.next_to(ax.c2p(6, 18), RIGHT)
        text_1.next_to(ax.c2p(6, 18), RIGHT)

        #text_a0 = Tex(f"a0 = {a0:.3f}", color=BLACK)
        #text_a0.next_to(ax.c2p(0.5, 50), RIGHT)
        #text_b0 = Tex(f"b0 = {b0:.3f}", color=BLACK)
        #text_b0.next_to(ax.c2p(0.5, 40), RIGHT)
        #text_e0 = Tex(f"e0 = {e0:.3f}", color=BLACK)
        #text_e0.next_to(ax.c2p(0.5, 30), RIGHT)

        dec_a.next_to(text_a, direction=RIGHT)
        dec_b.next_to(text_b, direction=RIGHT)
        dec_e.next_to(text_e, direction=RIGHT)
        dec_a.add_updater(lambda d: lineUpdaterA(d))
        dec_b.add_updater(lambda d: lineUpdaterB(d))
        dec_e.add_updater(lambda d: lineUpdaterE(d))

        splash = Text("Linear regression", font="sans-serif", color=BLACK)
        info   = Text("(Click to play video)", font="sans-serif", color=BLACK)
        splash.shift(2*UP)
        info.shift(2*DOWN)
        #self.play(Write(splash))
        self.add(splash,info)
        self.wait(1)
        self.remove(info)
        self.wait(1)
        self.play(Transform(splash, splash.copy().shift(1.5*UP)))

        self.play(Create(ax))
        self.play(Create(labels))
        self.play(Create(dot_group))

        self.wait(1)
        # Use the list of animations to play dots falling simultaneously
        self.play(LaggedStart(*rising_dots, lag_ratio=0.1))
        self.play(Create(text_0))
        self.wait(1)
        self.play(Create(mlines[0])); self.wait(1)
        self.add(text_a, text_b, text_e, text_eq)
        #self.add(text_a0, text_b0, text_e0)
        self.add(dec_a, dec_b, dec_e)
        self.wait(1)
        for i in range(1, len(mlines)-1):
            self.play(ReplacementTransform(mlines[i-1], mlines[i]));
            self.wait(1);
            self.current_line = mlines[i]
        self.remove(text_0)
        self.play(Create(text_1))
        self.wait(1);
        liner = mlines[-1]
        liner.set_stroke_width(2*liner.get_stroke_width())
        #liner.set_width(2*liner.get_width())
        self.play( ReplacementTransform(mlines[-2], liner), rate_func=linear, run_time=4);
        self.current_line = liner
        self.wait(5)

# ==============================================================================
#
# Main
#
# ==============================================================================

if __name__=='__main__':
    print("Hello :)")
    print("Useage:")
    print("manim -pql scene.py SquareToCircle")

